# Drone Commands API

O objetivo da API de comandos do drone é fornecer uma interface REST configurável para executar comandos de rotas do drone no simulador [Sphinx](https://developer.parrot.com/docs/sphinx/firststep.html). Através da API é possível cadastrar comandos disponibilizados nos scripts de interface com o drone [drone-routes](https://bitbucket.org/ggleme/drone-routes/).

Na API também foi desenvolvido um esquema de autenticação utilizando token JWT, porém, a implementação ainda depende de um modelo de autenticação da Alexa, que não foi desenvolvido.

## Instalação

As instruções abaixo foram testadas em sistemas operacionais Linux (Ubuntu/Debian) e MacOS, porém, para compatibilidade com os scripts de execução da simulação do drone, é necessário realizar o setup em um SO compatível com o simulador Sphinx e a SDK do Drone [Olympe](https://developer.parrot.com/docs/olympe/userguide.html) e cadastrar os scripts em python no banco de dados.

### Pré-requisitos

- Instalação do [Git](https://git-scm.com/downloads)
- Instalação do [Nodejs 12+](https://nodejs.org/en)
- Instalação do [MongoDB](https://docs.mongodb.com/manual/installation/) de acordo com seu sistema operacional
- Alternativa para o [MongoDB em Docker](https://hub.docker.com/_/mongo): `docker run --name <nome-do-container> -p 27017:27017 mongo:4.2`
- Instalação do [ngrok](https://ngrok.com/)

### Instalação do ngrok
o ngrok fornece um servidor com um domínio público que serve como proxy reverso para redirecionar o tráfego de rede (TCP, HTTP, HTTPS, entre outros) para o cliente onde é instalado. Para ambientes de produção, são necessárias maiores configurações para restringir o acesso da API em nível de rede, porém, para o experimento do drone, o ngrok se mostra muito eficaz por sua facilidade de instalação e configuração.

Crie uma conta no [site](https://ngrok.com/) do ngrok:

![01-signup](./docs/images/01-signup.png)

Realize o tutorial de instalação e configuração de acordo com o sistema operacional, porém, ao invés de expor a porta 80, exporte a porta que deseja executar o servidor HTTP da API.

![02-configure](./docs/images/02-configure.png)

Segue um exemplo de configuração local expondo a porta 3000 para requisições HTTP:

![03-run](./docs/images/03-run.png)


### Instalação da API

Clone o repositório git e copie o arquivo de exemplo de configuração das variáveis de ambiente:

```
git clone https://ggleme@bitbucket.org/ggleme/drone-commands-api.git

cd drone-commands-api

npm install

cp sample.env .env
```

O arquivo de configuração das variáveis de ambiente deve se parecer com o abaixo (de acordo com a disponibilidade de portas e configurações de banco de dados):

```.env
NODE_ENV=development
PORT=3000

MONGODB_PREFIX=mongodb
MONGODB_HOSTNAME=localhost
MONGODB_PORT=27017
MONGODB_DATABASE=drone-inspection
MONGODB_AUTH_DATABASE=drone-inspection
MONGODB_USERNAME=iot
MONGODB_PASSWORD=iot@2020
MONGODB_DEBUG=true

AUTHENTICATION_SECRET=U_ftgwxT-4@AH.V2HEhC.-L*UGLriVHGQBmzsdoK2Yd8ep64j9
AUTHENTICATION_EXPIRATION=30d
AUTHENTICATION_SALT_ROUNDS=10
```

## Executando a API

Para iniciar o servidor, após a configuração das variáveis de ambiente, execute o comando abaixo:

```
npm start
```

A API ira inciar um servidor HTTP que irá responder à requisições enviadas na porta específicada de acordo com a variável de ambiente `PORT`.

![04-api-start](docs/images/04-api-start.png)

## Criando um Comando Para Execução de uma Rota

Para que a API crie um novo processo e execute um comando de acordo com os scripts fornecidos no repositório de rotas do drone ([drone-routes](https://bitbucket.org/ggleme/drone-routes/)), é necessário cadastrar um comando na API através de uma requisição `POST` no endpoint `/commands`.

Tal comando será salvo no banco de dados, e a cada requisição `POST` para o endpoint `/commands/<SLOT>/invoke`, um novo processo do comando é executado.

Como exemplo, realizamos a implementação do comando de `INICIAR` inspeção, assim, o cadastro do comando pode ser realizado o comando [curl](https://curl.haxx.se/) abaixo:

```
curl --location --request POST 'https://localhost:3000/commands' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Start Inspection",
    "code": "INICIAR",
    "executablePath": "bash",
    "arguments": [
        "/opt/olympe-parrot/start_inspect.sh"
    ]
}'
```

A documentação completa da API esta disponibilizada em uma [collection](./docs/drone-commands-api.postman_collection.json) que pode ser importada através da ferramenta [Postman](https://www.postman.com/), e permite configurar dinamicamente o hostname da API e também possui a especificação de funcionamento dos métodos de autenticação.

## Testando a Integração com o Skill da Alexa

Para garantir o funcionamento de todo o mecanismo de entrega de mensagens e execução de comandos, o deploy da função lambda [drone-commands-dispatcher](https://bitbucket.org/ggleme/drone-commands-dispatcher/src) deve especificar a URL gerada através do ngrok através da variável de ambiente `DRONE_COMMANDS_API_BASE_URL`.

Finalmente, realize a chamada do Skill [drone-inspection-skill](https://bitbucket.org/ggleme/drone-inspection-skill) através do console de desenvolvimento ou através do aplicativo Amazon Alexa para [Android](https://play.google.com/store/apps/details?id=com.amazon.dee.app&hl=en) ou [iOS](https://apps.apple.com/us/app/amazon-alexa/id944011620) e verifique a requisição enviada para API através do [dashboard](http://localhost:4040) disponibilizado pelo ngrok.
